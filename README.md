# shChat Bridge

A Peer to Peer bridge between Discord and sshChat using [Hyper-Protocol](https://docs.holepunch.to/)


A high-level API for finding and connecting to peers who are interested in a "topic." 

[@hyperswarm/dht](https://docs.holepunch.to/building-blocks/hyperswarm#dht): 

A DHT powering Hyperswarm. Through this DHT, each server is bound to a unique key pair, with the client connecting to the server using the server's public key.


[@hyperswarm/secretstream](https://docs.holepunch.to/building-blocks/hyperswarm#secretstream): 

Secretstream is used to securely create connections between two peers in Hyperswarm.

## What is HyperSwarm?
Hyperswarm allows you to find and connect to peers who are announcing a common "topic". The swarm topic can be anything. This means that, with Hyperswarm, you can discover and connect peers with a shared interest over a distributed network. As an example, we often use Hypercore's discovery key as the swarm topic for discovering peers to replicate with.
Once Hyperswarm has discovered peers, nodes in the DHT help those peers connect via a UDP-based hole-punching algorithm that's designed to work well in home networks.

https://docs.holepunch.to/building-blocks/hyperswarm

## Usage

To install:

`git clone git@git.codingvm.codes:snxraven/sshChatBridge.git`

`cd sshChatBridge`

`npm i`

To run a new topic (room):

`node bot.js`

This will log the bot into Discord.


To set a topic use the !set-topic command within the channel you wish to bridge.

The bot will provide a connection topic hash to connect to.

Once a peer connects all messages will be sent both ways.

!leave [TOPIC HASH] will destroy a topic bridge.


